# Minimalist Iptables for Docker

## Overview

When exposing a port to a docker service on a system, the default behavior is
for docker to open that port to the wold in iptables.  This may be desirable
for many services, but not all.  One option is to disable the iptables
management feature of docker, and manually edit the configuration.  This is
possible, but presents many new challenges in trying to recreate all of the
logic that docker already has built in.  
Our solution to this issue is the following:

1. Run docker services on the loopback address
2. Expose services by having IPTable allow specific external IP ranges in using
   the nat table and PREROUTING chain

This project enables the iptables manipulation through an easy to use python script.

## Docker Configuration

Set your docker services to be localhost only.  This prevents modification of
public inbound tables

For example, if your old completely open docker service looked like this in docker-compose:

```
...
    ports:
      - "443:443"
      - "22:22"
...
```

It should now look like this:

```
...
    ports:
      - "127.0.0.1:443:443"
      - "127.0.0.1:22:22"
...
```

## Incoming Network Configuration

The script uses a custom yaml file for configuration.  Configuration looks like this:

```yaml
---
'443':
  - '0.0.0.0/0'
'22':
  - '192.168.1.0/24'
  - '1.2.3.4/32'
```

In the example above, services on port 443 would be open to the world, but
services on port 22 would only be open to the network `192.168.1.0/24`, and the
host `1.2.3.4`

## Running the script

Starting rules:

```
$ minimalist-iptables-docker.py start path_to_rules.yaml
```

Stopping rules:

```
$ minimalist-iptables-docker.py stop
```

**NOTE:** Right now, the stopping process removes pretty much any non-ufw ruleset,
so use with caution
