#!/usr/bin/env python3
"""

To create rules, you need a properly formatted rules file, it should look
something like this:

---
'9304':
  - 152.3.100.240
'9305':
  - 152.3.100.250
  - 152.3.100.251

Note that the port, maps to multiple hosts, which will then be allowed.

To prevent the default-openness of Docker iptables, ensure that your docker
services are listening on localhost only

"""

import iptc
import subprocess
import sys
import argparse
import yaml
import shutil


def parse_args():

    parser = argparse.ArgumentParser(
        description=
        "Insert or delete iptables rules to help restrict docker connections")

    subparsers = parser.add_subparsers(dest='action', help='sub-command help')
    subparsers.required = True
    parser_start = subparsers.add_parser('start',
                                         help='Start new iptables rules')
    parser_start.add_argument('rules_file', type=argparse.FileType('r'))
    subparsers.add_parser('stop', help='Remove all the old rules')

    return parser.parse_args()


def sysctl(label, value):
    sysctl_bin = shutil.which('sysctl')
    _, existing_value = subprocess.check_output([sysctl_bin, label
                                                 ]).decode().strip().split("=")
    existing_value = int(existing_value.strip())

    if existing_value != value:
        print("Setting %s to %s" % (label, value))
        subprocess.call([sysctl_bin, "-w", "%s=%s" % (label, value)])


def main():

    args = parse_args()

    if args.action == 'start':
        sysctl('net.ipv4.conf.eth0.route_localnet', 1)
        # Load in rules
        rules_data = yaml.safe_load(args.rules_file.read())

        # Get NAT working so containers can talk out
        table = iptc.Table(iptc.Table.FILTER)
        nat_table = iptc.Table(iptc.Table.NAT)

        # Autocommit is wonky, turn it off for now
        table.autocommit = False
        nat_table.autocommit = False

        chain = iptc.Chain(table, 'INPUT')
        pre_chain = iptc.Chain(nat_table, 'PREROUTING')
        for port, hosts in rules_data.items():
            # iptables -t nat -I PREROUTING -p tcp --dport 9304 -j DNAT --to-destination 127.0.0.1:9304
            rule = iptc.Rule()
            rule.protocol = "tcp"
            match = iptc.Match(rule, "tcp")
            match.dport = port
            rule.add_match(match)
            t = rule.create_target('DNAT')
            t.to_destination = '127.0.0.1:%s' % port
            print("Inserting: %s" % iptc.easy.decode_iptc_rule(rule))
            pre_chain.insert_rule(rule)

            for host in hosts:
                # iptables -t filter -A INPUT -s 10.182.1.69/32 -p tcp -m tcp --dport 9304 -j ACCEPT
                rule = iptc.Rule()
                target = iptc.Target(rule, "ACCEPT")
                rule.protocol = "tcp"
                match = iptc.Match(rule, "tcp")
                match.dport = port
                rule.src = host
                rule.add_match(match)
                rule.target = target

                print("Inserting: %s" % iptc.easy.decode_iptc_rule(rule))
                chain.insert_rule(rule)

        table.commit()
        nat_table.commit()

    elif args.action == 'stop':
        sysctl('net.ipv4.conf.eth0.route_localnet', 0)
        table = iptc.Table(iptc.Table.FILTER)
        nat_table = iptc.Table(iptc.Table.NAT)
        table.autocommit = False
        nat_table.autocommit = False

        # Input rules
        chain = iptc.Chain(table, 'INPUT')
        for rule in chain.rules:
            in_rule = iptc.easy.decode_iptc_rule(rule)
            # Skip ufw
            if in_rule['target'].startswith('ufw'):
                continue

            print("Deleting rule: %s" % in_rule)
            chain.delete_rule(rule)

        # Forwarding Rules
        chain = iptc.Chain(table, 'FORWARD')
        for rule in chain.rules:
            hr_rule = iptc.easy.decode_iptc_rule(rule)
            if 'in-interface' in hr_rule and hr_rule[
                    'in-interface'] == 'docker0':
                print("Deleting %s" % hr_rule)
                chain.delete_rule(rule)
            elif 'out-interface' in hr_rule and hr_rule[
                    'out-interface'] == 'docker0':
                print("Deleting %s" % hr_rule)
                chain.delete_rule(rule)

        # Postrouting rules
        chain = iptc.Chain(nat_table, 'POSTROUTING')
        for rule in chain.rules:
            hr_rule = iptc.easy.decode_iptc_rule(rule)
            if hr_rule['target'] == 'MASQUERADE':
                print("Deleting rule: %s" % hr_rule)
                chain.delete_rule(rule)

        # Prerouting rules
        chain = iptc.Chain(nat_table, 'PREROUTING')
        for rule in chain.rules:
            hr_rule = iptc.easy.decode_iptc_rule(rule)
            if hr_rule['target'] == 'DNAT':
                print("Deleting rule: %s" % hr_rule)
                chain.delete_rule(rule)

        table.commit()
        nat_table.commit()

    return 0


if __name__ == "__main__":
    sys.exit(main())
