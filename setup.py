import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name="minimalist-iptables-docker",
    version="0.0.1",
    author="Drew Stinnett",
    author_email="drew.stinnett@duke.edu",
    description=("Use minimal Iptables opening with docker"),
    license="BSD",
    keywords="docker iptables ufw",
    install_requires=requirements,
    packages=find_packages(),
    scripts=['scripts/minimalist-iptables-docker.py'],
    long_description=read('README.md'),
)
